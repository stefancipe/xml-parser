package com.primeholding.internship;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "departments" )
@XmlAccessorType (XmlAccessType.FIELD)
public class Departments {
	@XmlElement( name = "department" )
	protected List<Department> departments;

	public List<Department> getDepartments() {
		return departments;
	}

	public void setDepartments(List<Department> departments) {
		this.departments = departments;
	}

	@Override
	public String toString() {
		return "\nDepartments : [" + departments + "]";
	}
}
