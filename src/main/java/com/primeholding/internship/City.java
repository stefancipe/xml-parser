package com.primeholding.internship;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "city" )
@XmlAccessorType (XmlAccessType.FIELD)
public class City {
	@XmlAttribute
	protected String name;
	
	@XmlElement(name = "departments")
	protected Departments departments;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Departments getDepartments() {
		return departments;
	}
	public void setDepartments(Departments departments) {
		this.departments = departments;
	}
	@Override
	public String toString() {
		return "\nCity : [" + name + "," + departments + "]";
	}	
}
