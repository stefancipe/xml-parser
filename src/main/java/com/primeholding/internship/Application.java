package com.primeholding.internship;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class Application {

	public static void parseXmlFile(String file) throws JAXBException, FileNotFoundException {
		JAXBContext jaxbContext = JAXBContext.newInstance(Cities.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		
		
		InputStream inStream = new FileInputStream(file);
		Cities cities = (Cities) jaxbUnmarshaller.unmarshal(inStream);
		System.out.println(cities.toString());
	}
	
	public static void main(String[] args) {
		try {
			parseXmlFile("src/main/resources/2018-10-01-agivu.xml");
		} catch (FileNotFoundException | JAXBException e) {
			e.printStackTrace();
		}
	}

}
