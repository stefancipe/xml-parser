package com.primeholding.internship;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "department" )
@XmlAccessorType (XmlAccessType.FIELD)
public class Department {
	@XmlAttribute
	protected String name;
	@XmlElement(name = "employee")
	protected String employee;
	@XmlElement(name = "turnover")
	protected double turnover;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmployee() {
		return employee;
	}
	public void setEmployee(String employee) {
		this.employee = employee;
	}
	public double getTurnover() {
		return turnover;
	}
	public void setTurnover(double turnover) {
		this.turnover = turnover;
	}
	@Override
	public String toString() {
		return "\nDepartment : [name=" + name + ", employee=" + employee + ", turnover=" + turnover + "]";
	}
}
