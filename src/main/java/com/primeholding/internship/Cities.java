package com.primeholding.internship;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;

@XmlRootElement(name = "cities")
@XmlAccessorType (XmlAccessType.FIELD)
public class Cities {
	@XmlElement( name = "city" )
	private List<City> cities;

	public List<City> getCities() {
		return cities;
	}
	
	public void setCities(List<City> cities) {
		this.cities = cities;
	}

	@Override
	public String toString() {
		return "Cities : [" + cities + "]";
	}
}
